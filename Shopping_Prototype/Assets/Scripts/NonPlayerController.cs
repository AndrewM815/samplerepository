﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using UnityStandardAssets.Characters.ThirdPerson;

public class NonPlayerController : MonoBehaviour
{
    public Camera cam;
    public NavMeshAgent agent;
    public ThirdPersonCharacter character;
    public Transform walkTarget1;
    public Transform walkTarget2;
    public Transform walkTarget3;
    public Transform walkTarget4;
    public Transform walkTarget5;
    public Transform walkTarget6;
    public Transform walkTarget7;
    public Transform walkTarget8;
    public Transform walkTarget9;
    public Transform walkTarget10;

    public float waitDuration;
    bool firstTime;

    void Start()
    {
        firstTime = true;
        agent.updateRotation = false;
        StartCoroutine(NPCNewPos());
    }

    //public int MouseButtonToGo;

    void Update()
    {
     
        /*if (Input.GetMouseButtonDown(MouseButtonToGo))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                // Move the agent
                agent.SetDestination(hit.point);
            }

        }*/

        
        if (agent.remainingDistance > agent.stoppingDistance)
        {
            character.Move(agent.desiredVelocity, false, false);
            
        }
        else
        {
            character.Move(Vector3.zero, false, false);
            StartCoroutine(NPCNewPos());
        }
        
    }

    IEnumerator NPCNewPos()
    {
        

        if(!firstTime)
        {
            yield return new WaitForSeconds(Random.Range(waitDuration/2,waitDuration*2));
        }

        int RandomPos = Random.Range(1, 11);
        firstTime = false;


        if (RandomPos == 1)
        {
            agent.SetDestination(walkTarget1.position);
        }
        else if (RandomPos == 2)
        {
            agent.SetDestination(walkTarget2.position);
        }
        else if (RandomPos == 3)
        {
            agent.SetDestination(walkTarget3.position);
        }
        else if (RandomPos == 4)
        {
            agent.SetDestination(walkTarget4.position);
        }
        else if (RandomPos == 5)
        {
            agent.SetDestination(walkTarget5.position);
        }
        else if (RandomPos == 6)
        {
            agent.SetDestination(walkTarget6.position);
        }
        else if (RandomPos == 7)
        {
            agent.SetDestination(walkTarget7.position);
        }
        else if (RandomPos == 8)
        {
            agent.SetDestination(walkTarget8.position);
        }
        else if (RandomPos == 9)
        {
            agent.SetDestination(walkTarget9.position);
        }
        else if (RandomPos == 10)
        {
            agent.SetDestination(walkTarget10.position);
        }

    }
}
