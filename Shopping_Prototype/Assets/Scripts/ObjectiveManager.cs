﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.ParticleSystemJobs;

public class ObjectiveManager : MonoBehaviour
{
    public GameObject BoxUIText; //yellow
    public GameObject CansUIText; //red
    public GameObject BottlesUIText; //green    
    public GameObject CartonsUIText; //blue

    public int ItemsHere;
    public int BoxesNeeded; 
    public int BoxesHere;
    public int CansNeeded;
    public int CansHere;
    public int BottlesNeeded;
    public int BottlesHere;
    public int CartonsNeeded;
    public int CartonsHere;

    public GameObject[] itemDeliveryLocations;

    public int score;

    public GameObject ParticleObjects1;
    public GameObject ParticleObjects2;
    public GameObject ParticleObjects3;
    public GameObject ParticleObjects4;

    private void Start()
    {
        NewObjective();
    }

    void Update()
    {
        CansUIText.GetComponent<Text>().text = CansNeeded + "";
        BottlesUIText.GetComponent<Text>().text = BottlesNeeded + "";
        BoxUIText.GetComponent<Text>().text = BoxesNeeded + "";
        CartonsUIText.GetComponent<Text>().text = CartonsNeeded + "";

        if(ItemsHere == 10)
        {
            NewObjective();
            score += 1;

            ParticleObjects1.GetComponent<ParticleSystem>().Play();
            ParticleObjects2.GetComponent<ParticleSystem>().Play();
            ParticleObjects3.GetComponent<ParticleSystem>().Play();
            ParticleObjects4.GetComponent<ParticleSystem>().Play();
        }
    }

    void NewObjective()
    {
        BoxesNeeded = Random.Range(0, 4);
        BottlesNeeded = Random.Range(0, 4);
        CartonsNeeded = Random.Range(0, 4);;

        CansNeeded = (10 - BoxesNeeded - BottlesNeeded - CartonsNeeded);

        CartonsHere = 0;
        CansHere = 0;
        BoxesHere = 0;
        BottlesHere = 0;

        ItemsHere = 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Bottle"))
        {
            //other.gameObject.transform.SetParent(itemDeliveryLocations[ItemsHere].transform);
            //other.gameObject.transform.localPosition = new Vector3(0, 0, 0);
            if(BottlesNeeded >= 1)
            {
                ItemsHere += 1;
                BottlesHere += 1;
                BottlesNeeded -= 1;
            }
            
            Destroy(other.gameObject);

        }
        else if (other.CompareTag("Box"))
        {
            //other.gameObject.transform.SetParent(itemDeliveryLocations[ItemsHere].transform);
            //other.gameObject.transform.localPosition = new Vector3(0, 0, 0);

            if (BoxesNeeded >= 1)
            {
                ItemsHere += 1;
                BoxesHere += 1;
                BoxesNeeded -= 1;
            }
            Destroy(other.gameObject);

        }
        else if (other.CompareTag("Can"))
        {
            //other.gameObject.transform.SetParent(itemDeliveryLocations[ItemsHere].transform);
            //other.gameObject.transform.localPosition = new Vector3(0, 0, 0);

            if(CansNeeded >= 1)
            {
                ItemsHere += 1;
                CansHere += 1;
                CansNeeded -= 1;
            }

            Destroy(other.gameObject);

        }
        else if (other.CompareTag("Carton"))
        {
            //other.gameObject.transform.SetParent(itemDeliveryLocations[ItemsHere].transform);
            //other.gameObject.transform.localPosition = new Vector3(0, 0, 0);

            if (CartonsNeeded >= 1)
            {
                ItemsHere += 1;
                CartonsHere += 1;
                CartonsNeeded -= 1;
            }

            Destroy(other.gameObject);

        }

    }
}
