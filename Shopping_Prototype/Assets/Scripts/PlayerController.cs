﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using UnityStandardAssets.Characters.ThirdPerson;

public class PlayerController : MonoBehaviour
{
    public Camera cam;
    public NavMeshAgent agent;
    public ThirdPersonCharacter character;

    public GameObject itemToGrab;
    public GameObject shelfArea;

    public GameObject ProgressUI;
    public float ProgressUIfloat;
    public GameObject ProgressUIfloatParent;
    public int itemsLeftToPickUp;
    public GameObject itemsLeftToPickUpUIParent;

    public bool progressBarGo;

    public float pickUpItemSpeed;

    void Start()
    {
        agent.updateRotation = false;
        ProgressUIfloat = 0;

        //TEMP
        //itemsLeftToPickUp = 5;
    }

    public int MouseButtonToGo;

    //basket sizes (max 6), boxes 6 each, cartons 3 each, bottles 2 each, cans 1 each

    void Update()
    {
        ProgressUIfloatParent.GetComponent<Image>().fillAmount = ProgressUIfloat;
        itemsLeftToPickUpUIParent.GetComponent<Text>().text = itemsLeftToPickUp + "";
        
        if(shelfArea != null)
        {
            itemsLeftToPickUp = shelfArea.transform.parent.gameObject.GetComponent<ShelfSingle>().hoveredShelfRemainingItems;
        }
        else
        {
            itemsLeftToPickUp = 0;
        }

        if (Input.GetMouseButtonDown(MouseButtonToGo))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                // Move the agent
                agent.SetDestination(hit.point);
            }

        }

        if(Input.GetKeyDown(KeyCode.Space))
        {
            if(itemToGrab != null && shelfArea != null && itemsLeftToPickUp >= 1)
            {
                progressBarGo = true;
                //itemToGrab.SetActive(false);
            }
            else
            {
                progressBarGo = false;
            }
        }

        if(progressBarGo && itemsLeftToPickUp >= 1)
        {
            ProgressUIfloat += pickUpItemSpeed * Time.deltaTime;
            ProgressUI.GetComponent<Animator>().SetBool("ProgressUIDisplay", true);


            if (ProgressUIfloat >= 1)
            {
                ProgressUIfloat = 0;
                //itemsLeftToPickUp -= 1;
                shelfArea.transform.parent.gameObject.GetComponent<ShelfSingle>().itemLifted();
            }
        }
        else
        {
            ProgressUI.GetComponent<Animator>().SetBool("ProgressUIDisplay", false);
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            progressBarGo = false;
            ProgressUI.GetComponent<Animator>().SetBool("ProgressUIDisplay", false);
            ProgressUIfloat = 0;
        }

        if (agent.remainingDistance > agent.stoppingDistance)
        {
            character.Move(agent.desiredVelocity, false, false);
            
        }
        else
        {
            character.Move(Vector3.zero, false, false);
        }
        
    }
}
