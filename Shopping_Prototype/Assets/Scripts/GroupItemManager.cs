﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupItemManager : MonoBehaviour
{
    public List<GameObject> items;
    public int amountOfItemsMax;
    public int amountOfItemsRemaining;

    public GameObject PlayerBasketObject;


    void Start()
    {
        foreach (Transform child in transform)
        {
            items.Add(child.gameObject);
        }

        amountOfItemsMax = items.Count;

        PlayerBasketObject = gameObject.transform.parent.gameObject.transform.parent.GetComponent<ShelfSingle>().basketObject;

    }

    void Update()
    {
        amountOfItemsRemaining = items.Count;
    }

    public void itemLiftedSingle()
    {
        //items[0].SetActive(false);
        items[0].transform.SetParent(PlayerBasketObject.transform);
        items[0].transform.localPosition = new Vector3(0, 0, 0);
        items.Remove(items[0]);
        items.TrimExcess();
    }



    }
