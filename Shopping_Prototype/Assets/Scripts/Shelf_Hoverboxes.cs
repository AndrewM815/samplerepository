﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shelf_Hoverboxes : MonoBehaviour
{
    GameObject parentShelf;
    public bool isTopShelf;
    public bool isOnLeft;

    private void Start()
    {
        parentShelf = gameObject.transform.parent.gameObject;
    }

    void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            parentShelf.GetComponent<ShelfSingle>().PlayerHere = true;
            other.gameObject.GetComponent<PlayerController>().shelfArea = gameObject;
            other.gameObject.GetComponent<PlayerController>().itemToGrab = parentShelf.GetComponent<ShelfSingle>().objectToGrab;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            parentShelf.GetComponent<ShelfSingle>().PlayerHere = false;

            if(other.gameObject.GetComponent<PlayerController>().shelfArea == gameObject)
            {
                other.gameObject.GetComponent<PlayerController>().shelfArea = null;
            }
        }
    }

    void OnMouseOver()
    {
        gameObject.GetComponent<Renderer>().enabled = true;
        parentShelf.GetComponent<ShelfSingle>().StandHoverShelf(isTopShelf, isOnLeft);
    }

    void OnMouseExit()
    {
        gameObject.GetComponent<Renderer>().enabled = false;
        parentShelf.GetComponent<ShelfSingle>().StopHoverShelf(isTopShelf, isOnLeft);
    }
}
