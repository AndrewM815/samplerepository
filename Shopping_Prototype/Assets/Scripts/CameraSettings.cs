﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSettings : MonoBehaviour
{
    public float cameraRotateSpeed;
    float rotateY;
    bool movingLeft;
    bool movingRight;


    private void Start()
    {
        rotateY = gameObject.transform.rotation.y;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
        {
            movingLeft = true;
        }

        if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.LeftArrow))
        {
            movingLeft = false;
        }

        if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
        {
            movingRight = true;
        }

        if (Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.RightArrow))
        {
            movingRight = false;
        }

        if (movingLeft)
        {
            transform.Rotate(Vector3.up * (cameraRotateSpeed * 10) * Time.deltaTime, Space.Self);
        }

        if (movingRight)
        {
            transform.Rotate(Vector3.down * (cameraRotateSpeed * 10) * Time.deltaTime, Space.Self);
        }

    }
}
