﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShelfSingle : MonoBehaviour
{
    public GameObject BoxParentPrefab;
    public GameObject CartonParentPrefab;
    public GameObject CanParentPrefab;
    public GameObject BottleParentPrefab;

    public GameObject shelfTopLeftPos;
    public GameObject shelfTopRightPos;
    public GameObject shelfBottomLeftPos;
    public GameObject shelfBottomRightPos;

    public GameObject topLeftShelfContents;
    public GameObject topRightShelfContents;
    public GameObject BottomLeftShelfContents;
    public GameObject BottomRightShelfContents;

    public GameObject hoveringOver;

    public GameObject objectToGrab;
    public GameObject PlayerCharacter;

    public Material CanMat1; public Material CanMat2; public Material CanMat3;
    public Material CartonMat1; public Material CartonMat2; public Material CartonMat3;
    public Material BottleMat1; public Material BottleMat2; public Material BottleMat3;
    public Material BoxMat1; public Material BoxMat2; public Material BoxMat3;

    public bool PlayerHere;

    private void Start()
    {
        PopulateShelves();
    }

    public void StandHoverShelf(bool isTopShelf, bool isOnLeft)
    {
        if (PlayerHere)
        {
            if (isTopShelf)
            {
                if (isOnLeft)
                {
                    topLeftShelfContents.GetComponent<Animator>().SetBool("HoverMe", true);
                    objectToGrab = topLeftShelfContents;
                }
                else
                {
                    topRightShelfContents.GetComponent<Animator>().SetBool("HoverMe", true);
                    objectToGrab = topRightShelfContents;

                }
            }
            else
            {
                if (isOnLeft)
                {
                    BottomLeftShelfContents.GetComponent<Animator>().SetBool("HoverMe", true);
                    objectToGrab = BottomLeftShelfContents;

                }
                else
                {
                    BottomRightShelfContents.GetComponent<Animator>().SetBool("HoverMe", true);
                    objectToGrab = BottomRightShelfContents;
                }
            }

        }
    
    }

    public void StopHoverShelf(bool isTopShelf, bool isOnLeft)
    {
        //if (PlayerHere)
        //{
            if (isTopShelf)
            {
                if (isOnLeft)
                {
                    topLeftShelfContents.GetComponent<Animator>().SetBool("HoverMe", false);
                }
                else
                {
                    topRightShelfContents.GetComponent<Animator>().SetBool("HoverMe", false);
                }
            }
            else
            {
                if (isOnLeft)
                {
                    BottomLeftShelfContents.GetComponent<Animator>().SetBool("HoverMe", false);
                }
                else
                {
                    BottomRightShelfContents.GetComponent<Animator>().SetBool("HoverMe", false);
                }
            }

        objectToGrab = null;

        //}
    }

    public void PopulateShelves()
    {
        //random for now 
        //later maybe grab the amount of each object in a scene and then base objectives on that
        int randomNumberShelf1 = Random.Range(0, 4);
        int randomNumberShelf2 = Random.Range(0, 4);
        int randomNumberShelf3 = Random.Range(0, 4);
        int randomNumberShelf4 = Random.Range(0, 4);

        if (randomNumberShelf1 == 0)
        {
            topLeftShelfContents = Instantiate(BoxParentPrefab, shelfTopLeftPos.transform);

            foreach (Transform BoxChild in topLeftShelfContents.transform)
            {
                int randomNumBoxMat = Random.Range(0, 3);

                if (randomNumBoxMat == 0)
                {
                    BoxChild.GetComponent<Renderer>().material = BoxMat1;
                }
                else if (randomNumBoxMat == 1)
                {
                    BoxChild.GetComponent<Renderer>().material = BoxMat2;
                }
                else if (randomNumBoxMat == 2)
                {
                    BoxChild.GetComponent<Renderer>().material = BoxMat3;
                }
            }
        }
        else if (randomNumberShelf1 == 1)
        {
            topLeftShelfContents = Instantiate(CartonParentPrefab, shelfTopLeftPos.transform);

            foreach (Transform CartonChild in topLeftShelfContents.transform)
            {
                int randomNumCartonMat = Random.Range(0, 3);

                if (randomNumCartonMat == 0)
                {
                    CartonChild.GetComponent<Renderer>().material = CartonMat1;
                }
                else if (randomNumCartonMat == 1)
                {
                    CartonChild.GetComponent<Renderer>().material = CartonMat2;
                }
                else if (randomNumCartonMat == 2)
                {
                    CartonChild.GetComponent<Renderer>().material = CartonMat3;
                }
            }
        }
        else if (randomNumberShelf1 == 2)
        {
            topLeftShelfContents = Instantiate(CanParentPrefab, shelfTopLeftPos.transform);

            foreach(Transform CanChild in topLeftShelfContents.transform)
            {
                int randomNumCanMat = Random.Range(0, 3);

                if(randomNumCanMat == 0)
                {
                    CanChild.GetComponent<Renderer>().material = CanMat1;
                }
                else if (randomNumCanMat == 1)
                {
                    CanChild.GetComponent<Renderer>().material = CanMat2;
                }
                else if (randomNumCanMat == 2)
                {
                    CanChild.GetComponent<Renderer>().material = CanMat3;
                }
            }
        }
        else if (randomNumberShelf1 == 3)
        {
            topLeftShelfContents = Instantiate(BottleParentPrefab, shelfTopLeftPos.transform);

            foreach (Transform BottleChild in topLeftShelfContents.transform)
            {
                int randomNumBottleMat = Random.Range(0, 3);

                if (randomNumBottleMat == 0)
                {
                    BottleChild.GetComponent<Renderer>().material = BottleMat1;
                }
                else if (randomNumBottleMat == 1)
                {
                    BottleChild.GetComponent<Renderer>().material = BottleMat2;
                }
                else if (randomNumBottleMat == 2)
                {
                    BottleChild.GetComponent<Renderer>().material = BottleMat3;
                }
            }
        }

        if (randomNumberShelf2 == 0)
        {
            topRightShelfContents = Instantiate(BoxParentPrefab, shelfTopRightPos.transform);

            foreach (Transform BoxChild in topRightShelfContents.transform)
            {
                int randomNumBoxMat = Random.Range(0, 3);

                if (randomNumBoxMat == 0)
                {
                    BoxChild.GetComponent<Renderer>().material = BoxMat1;
                }
                else if (randomNumBoxMat == 1)
                {
                    BoxChild.GetComponent<Renderer>().material = BoxMat2;
                }
                else if (randomNumBoxMat == 2)
                {
                    BoxChild.GetComponent<Renderer>().material = BoxMat3;
                }
            }
        }
        else if (randomNumberShelf2 == 1)
        {
            topRightShelfContents = Instantiate(CartonParentPrefab, shelfTopRightPos.transform);

            foreach (Transform CartonChild in topRightShelfContents.transform)
            {
                int randomNumCartonMat = Random.Range(0, 3);

                if (randomNumCartonMat == 0)
                {
                    CartonChild.GetComponent<Renderer>().material = CartonMat1;
                }
                else if (randomNumCartonMat == 1)
                {
                    CartonChild.GetComponent<Renderer>().material = CartonMat2;
                }
                else if (randomNumCartonMat == 2)
                {
                    CartonChild.GetComponent<Renderer>().material = CartonMat3;
                }
            }
        }
        else if (randomNumberShelf2 == 2)
        {
            topRightShelfContents = Instantiate(CanParentPrefab, shelfTopRightPos.transform);

            foreach (Transform CanChild in topRightShelfContents.transform)
            {
                int randomNumCanMat = Random.Range(0, 3);

                if (randomNumCanMat == 0)
                {
                    CanChild.GetComponent<Renderer>().material = CanMat1;
                }
                else if (randomNumCanMat == 1)
                {
                    CanChild.GetComponent<Renderer>().material = CanMat2;
                }
                else if (randomNumCanMat == 2)
                {
                    CanChild.GetComponent<Renderer>().material = CanMat3;
                }
            }
        }
        else if (randomNumberShelf2 == 3)
        {
            topRightShelfContents = Instantiate(BottleParentPrefab, shelfTopRightPos.transform);

            foreach (Transform BottleChild in topRightShelfContents.transform)
            {
                int randomNumBottleMat = Random.Range(0, 3);

                if (randomNumBottleMat == 0)
                {
                    BottleChild.GetComponent<Renderer>().material = BottleMat1;
                }
                else if (randomNumBottleMat == 1)
                {
                    BottleChild.GetComponent<Renderer>().material = BottleMat2;
                }
                else if (randomNumBottleMat == 2)
                {
                    BottleChild.GetComponent<Renderer>().material = BottleMat3;
                }
            }
        }

        if (randomNumberShelf3 == 0)
        {
            BottomLeftShelfContents = Instantiate(BoxParentPrefab, shelfBottomLeftPos.transform);

            foreach (Transform BoxChild in BottomLeftShelfContents.transform)
            {
                int randomNumBoxMat = Random.Range(0, 3);

                if (randomNumBoxMat == 0)
                {
                    BoxChild.GetComponent<Renderer>().material = BoxMat1;
                }
                else if (randomNumBoxMat == 1)
                {
                    BoxChild.GetComponent<Renderer>().material = BoxMat2;
                }
                else if (randomNumBoxMat == 2)
                {
                    BoxChild.GetComponent<Renderer>().material = BoxMat3;
                }
            }
        }
        else if (randomNumberShelf3 == 1)
        {
            BottomLeftShelfContents = Instantiate(CartonParentPrefab, shelfBottomLeftPos.transform);

            foreach (Transform CartonChild in BottomLeftShelfContents.transform)
            {
                int randomNumCartonMat = Random.Range(0, 3);

                if (randomNumCartonMat == 0)
                {
                    CartonChild.GetComponent<Renderer>().material = CartonMat1;
                }
                else if (randomNumCartonMat == 1)
                {
                    CartonChild.GetComponent<Renderer>().material = CartonMat2;
                }
                else if (randomNumCartonMat == 2)
                {
                    CartonChild.GetComponent<Renderer>().material = CartonMat3;
                }
            }
        }
        else if (randomNumberShelf3 == 2)
        {
            BottomLeftShelfContents = Instantiate(CanParentPrefab, shelfBottomLeftPos.transform);

            foreach (Transform CanChild in BottomLeftShelfContents.transform)
            {
                int randomNumCanMat = Random.Range(0, 3);

                if (randomNumCanMat == 0)
                {
                    CanChild.GetComponent<Renderer>().material = CanMat1;
                }
                else if (randomNumCanMat == 1)
                {
                    CanChild.GetComponent<Renderer>().material = CanMat2;
                }
                else if (randomNumCanMat == 2)
                {
                    CanChild.GetComponent<Renderer>().material = CanMat3;
                }
            }
        }
        else if (randomNumberShelf3 == 3)
        {
            BottomLeftShelfContents = Instantiate(BottleParentPrefab, shelfBottomLeftPos.transform);

            foreach (Transform BottleChild in BottomLeftShelfContents.transform)
            {
                int randomNumBottleMat = Random.Range(0, 3);

                if (randomNumBottleMat == 0)
                {
                    BottleChild.GetComponent<Renderer>().material = BottleMat1;
                }
                else if (randomNumBottleMat == 1)
                {
                    BottleChild.GetComponent<Renderer>().material = BottleMat2;
                }
                else if (randomNumBottleMat == 2)
                {
                    BottleChild.GetComponent<Renderer>().material = BottleMat3;
                }
            }
        }

        if (randomNumberShelf4 == 0)
        {
            BottomRightShelfContents = Instantiate(BoxParentPrefab, shelfBottomRightPos.transform);

            foreach (Transform BoxChild in BottomRightShelfContents.transform)
            {
                int randomNumBoxMat = Random.Range(0, 3);

                if (randomNumBoxMat == 0)
                {
                    BoxChild.GetComponent<Renderer>().material = BoxMat1;
                }
                else if (randomNumBoxMat == 1)
                {
                    BoxChild.GetComponent<Renderer>().material = BoxMat2;
                }
                else if (randomNumBoxMat == 2)
                {
                    BoxChild.GetComponent<Renderer>().material = BoxMat3;
                }
            }
        }
        else if (randomNumberShelf4 == 1)
        {
            BottomRightShelfContents = Instantiate(CartonParentPrefab, shelfBottomRightPos.transform);

            foreach (Transform CartonChild in BottomRightShelfContents.transform)
            {
                int randomNumCartonMat = Random.Range(0, 3);

                if (randomNumCartonMat == 0)
                {
                    CartonChild.GetComponent<Renderer>().material = CartonMat1;
                }
                else if (randomNumCartonMat == 1)
                {
                    CartonChild.GetComponent<Renderer>().material = CartonMat2;
                }
                else if (randomNumCartonMat == 2)
                {
                    CartonChild.GetComponent<Renderer>().material = CartonMat3;
                }
            }
        }
        else if (randomNumberShelf4 == 2)
        {
            BottomRightShelfContents = Instantiate(CanParentPrefab, shelfBottomRightPos.transform);

            foreach (Transform CanChild in BottomRightShelfContents.transform)
            {
                int randomNumCanMat = Random.Range(0, 3);

                if (randomNumCanMat == 0)
                {
                    CanChild.GetComponent<Renderer>().material = CanMat1;
                }
                else if (randomNumCanMat == 1)
                {
                    CanChild.GetComponent<Renderer>().material = CanMat2;
                }
                else if (randomNumCanMat == 2)
                {
                    CanChild.GetComponent<Renderer>().material = CanMat3;
                }
            }
        }
        else if (randomNumberShelf4 == 3)
        {
            BottomRightShelfContents = Instantiate(BottleParentPrefab, shelfBottomRightPos.transform);

            foreach (Transform BottleChild in BottomRightShelfContents.transform)
            {
                int randomNumBottleMat = Random.Range(0, 3);

                if (randomNumBottleMat == 0)
                {
                    BottleChild.GetComponent<Renderer>().material = BottleMat1;
                }
                else if (randomNumBottleMat == 1)
                {
                    BottleChild.GetComponent<Renderer>().material = BottleMat2;
                }
                else if (randomNumBottleMat == 2)
                {
                    BottleChild.GetComponent<Renderer>().material = BottleMat3;
                }
            }
        }
    }
}
