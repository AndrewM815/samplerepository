﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupItemManager : MonoBehaviour
{
    public List<GameObject> items;
    public int amountOfItemsMax;
    public int amountOfItemsRemaining;


    void Start()
    {
        foreach (Transform child in transform)
        {
            items.Add(child.gameObject);
        }

        amountOfItemsMax = items.Count;

    }

    void Update()
    {
        amountOfItemsRemaining = items.Count;
    }



}
