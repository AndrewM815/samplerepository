﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityStandardAssets.Characters.ThirdPerson;

public class PlayerController : MonoBehaviour
{
    public Camera cam;
    public NavMeshAgent agent;
    public ThirdPersonCharacter character;

    public GameObject itemToGrab;
    public GameObject shelfArea;

    void Start()
    {
        agent.updateRotation = false;
    }

    public int MouseButtonToGo;

    void Update()
    {
        if(Input.GetMouseButtonDown(MouseButtonToGo))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                // Move the agent
                agent.SetDestination(hit.point);
            }

        }

        if(Input.GetKey(KeyCode.Space))
        {
            if(itemToGrab != null && shelfArea != null)
            {
                itemToGrab.SetActive(false);
            }
        }

        if(agent.remainingDistance > agent.stoppingDistance)
        {
            character.Move(agent.desiredVelocity, false, false);
        }
        else
        {
            character.Move(Vector3.zero, false, false);
        }

        
    }
}
