Shopping Prototype Mini-Project developed by Andrew McKeown

--- Install instructions ---

Navigate to "Builds" folder within "Shopping_Prototype" folder.
Then within the "Shopping_Level1" folder double-click on "Shopping_Prototype" to
launch the game.

--- Objective ---

Click to instruct green player on where to run to. Use keys "A" or "D" to rotate the camera.
The aim of the game is to add the requested shopping items to your basket and then return them to
the green delivery area. Hovering over the delivery area are the requested items
sorted by colour (e.g. 4 yellow, 2 red, 1 green and 1 blue means 4 yellow boxes, 
2 red cans, 1 green bottle, and 1 blue carton).

To pick up an item from its shelf you must navigate to stand in front of the shelf,
then hover over the item with the mouse - the item will float when selected. Then
hold spacebar to add the items to your basket 1 by 1.

Run back to the green delivery area to empty your basket and complete an objective.

--- Replicating the project ---

Most features were developed from the Unity Learn tutorial for NavMesh (https://learn.unity.com/tutorial/unity-navmesh).
Following this will explain NavMesh agents, obstacles and baking in full detail. It's important to note
obstacle priority on NavMesh agents as well so you don't get stuck while pushing through the crowds.
The 3D player model is included in that Unity Learn tutorial package, along with its animator.

The shelf, item and basket models were created in ProBuilder with the help of these tutorials
(https://learn.unity.com/tutorial/working-with-poly-shapes-in-probuilder, 
https://learn.unity.com/tutorial/working-with-shapes-in-probuilder).

Finally small animations (hovering shopping items) were developed using this tutorial 
(https://learn.unity.com/tutorial/working-with-animation-curves-2019-3).


